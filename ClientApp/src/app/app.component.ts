import { Component } from '@angular/core';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})
export class AppComponent {

    title = 'app';

    constructor(private titleService: Title) {
        this.setTitle("Vy FAQ");
    }

    public setTitle(newTitle: string) {
        this.titleService.setTitle(newTitle);
    }
}
