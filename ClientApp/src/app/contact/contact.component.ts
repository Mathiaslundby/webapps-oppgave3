import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Category } from '../models/category';
import { Question } from '../models/question';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { error } from 'util';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {

    email: string;
    question: string;
    selectedValue: number;
    contactForm: FormGroup;

    categories: Array<Category>;
    loading: boolean;
    sent: boolean;

    constructor(private _http: HttpClient, private fb: FormBuilder) {
        this.contactForm = fb.group({
            email: [null, Validators.compose([Validators.required, Validators.pattern("^[a-zA-Z._-]{2,}@[a-zA-Z-]{2,}.[a-zA-Z]{2,3}$")])],
            category: [null, Validators.compose([Validators.required, Validators.pattern("^[0-9]{1,}$")])],
            body: [null, Validators.compose([Validators.required])]
        });
    }

    ngOnInit() {
        this.sent = false;
        this.loading = true;
        this.loadCategories();
    }

    loadCategories() {
        this._http.get<Category[]>("api/categories")
            .subscribe(
                cats => {
                    this.categories = cats;
                    this.loading = false;
                },
                error => console.log(error)
            );
    }

    sendQuestion() {
        var q = new Question();

        q.Email = this.contactForm.value.email;
        q.Body = this.contactForm.value.body;
        q.CategoryId = this.contactForm.value.category;
        q.CategoryName = ""; //API gets name from ID

        const qBody: string = JSON.stringify(q);
        const headers = new HttpHeaders({ "Content-type": "application/json" });

        this._http.post("api/questions", qBody, { headers: headers })
            .subscribe(() => {
                this.contactForm.reset();
                this.sent = true;
            },
                error => {
                    this.sent = false;
                    console.log(error)
                }
        );
    }
}
