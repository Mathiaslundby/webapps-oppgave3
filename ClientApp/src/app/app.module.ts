import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { FaqComponent } from './faq/faq.component';
import { ContactComponent } from './contact/contact.component';
import { QuestionsComponent } from './questions/questions.component';
import { FrontpageComponent } from './frontpage/frontpage.component';

@NgModule({
  declarations: [
        AppComponent,
        FaqComponent,
        ContactComponent,
        QuestionsComponent,
        FrontpageComponent
  ],
  imports: [
      BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
      HttpClientModule,
      FormsModule,
      ReactiveFormsModule,
      RouterModule.forRoot([
          { path: '', component: FrontpageComponent},
          { path: 'faq', component: FaqComponent },
          { path: 'contact', component: ContactComponent },
          { path: 'questions', component: QuestionsComponent },
          { path: '**', redirectTo: '/' }
      ])
  ],
  bootstrap: [AppComponent],
  exports: [RouterModule]
})
export class AppModule { }
