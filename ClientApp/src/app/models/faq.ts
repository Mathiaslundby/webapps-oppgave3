export class Faq {
    id?: number;
    question: string;
    answer: string;
    categoryId: number;
    rating: number;
}

export class DisplayFaq {
    id?: number;
    question: string;
    answer: string;
    categoryId: number;
    rating: number;
    upvoted: boolean;
    downvoted: boolean;
}
