export class Question {
    ID?: number;
    Email: string;
    Body: string;
    CategoryId: number;
    CategoryName: string;
}
