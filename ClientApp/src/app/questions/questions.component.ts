import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Question } from '../models/question';

@Component({
  selector: 'app-questions',
  templateUrl: './questions.component.html',
  styleUrls: ['./questions.component.css']
})
export class QuestionsComponent implements OnInit {


    questions: Array<Question>;
    loading: boolean;
    sent: boolean;

    selectedId: number;
    selectedEmail: string;
    selectedCategory: string;
    selectedBody: string;

    constructor(private _http: HttpClient) { }

    ngOnInit() {
        this.loading = true;
        this.loadQuestions();
    }

    loadQuestions() {
        this._http.get<Question[]>("api/questions")
            .subscribe(
                qs => {
                    this.questions = qs;
                    this.loading = false;
                },
                error => console.log(error)
            );
    }

    delete(id: number) {
        this._http.delete("api/questions/" + id)
            .subscribe(() => {
                this.loadQuestions();
            })
    }

    setDeleteModal(id: number, email: string, category: string, body: string) {
        this.selectedId = id;
        this.selectedEmail = email;
        this.selectedCategory = category;
        this.selectedBody = body;
    }

}
