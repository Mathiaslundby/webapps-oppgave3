import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Category } from '../models/category';
import { Faq, DisplayFaq } from '../models/faq';
import { Vote } from '../models/vote';
import { error } from '@angular/compiler/src/util';
import { concat } from 'rxjs';


@Component({
  selector: 'app-faq',
  templateUrl: './faq.component.html',
  styleUrls: ['./faq.component.css']
})

export class FaqComponent implements OnInit {

    loading = true;

    categories: Array<Category>;
    faqList: Array<Faq>;
    DisplayFaqList: Array<DisplayFaq>;
    category: number;
    ratingElement: HTMLElement;

    constructor(private _http: HttpClient) {
          
    }

    ngOnInit() {
        this.DisplayFaqList = new Array();
        this.loadCategories();
        this.loadFaq();
    }

    loadCategories() {
        this._http.get<Category[]>("api/categories")
            .subscribe(
                cats => {
                    this.categories = cats;
                    this.loading = false;
                },
                error => console.log(error)
            );
    }

    loadFaq() {
        this._http.get<Faq[]>("api/faq")
            .subscribe(
                faq => {
                    this.faqList = faq;
                    this.setFaqList();
                    this.loading = false;
                },
                error => console.log(error)
            );
    }

    setFaqList() {
        for (let f of this.faqList) {
            let dFaq = new DisplayFaq();
            dFaq.id = f.id;
            dFaq.question = f.question;
            dFaq.answer = f.answer;
            dFaq.categoryId = f.categoryId;
            dFaq.rating = f.rating;
            dFaq.upvoted = false;
            dFaq.downvoted = false;
            this.DisplayFaqList.push(dFaq);
        }
    }

    onCategorySelected(value: number) {
        this.category = value;
    }

    vote(id: number, upvote: boolean) {

        let voteValue = 0;

        for (let f of this.DisplayFaqList) {
            if (f.id == id) {
                if (upvote && f.upvoted) {
                    voteValue = -1;
                    f.upvoted = false;
                    f.downvoted = false;
                }
                else if (!upvote && f.downvoted) {
                    voteValue = 1;
                    f.upvoted = false;
                    f.downvoted = false;
                }
                else if (upvote && (!f.upvoted && !f.downvoted)) {
                    voteValue = 1;
                    f.upvoted = true;
                    f.downvoted = false;
                }
                else if (!upvote && (!f.upvoted && !f.downvoted)) {
                    voteValue = -1;
                    f.downvoted = true;
                    f.upvoted = false;
                }
                else if (upvote && f.downvoted) {
                    voteValue = 2;
                    f.upvoted = true;
                    f.downvoted = false;
                }
                else if (!upvote && f.upvoted) {
                    voteValue = -2;
                    f.upvoted = false;
                    f.downvoted = true;
                }

                let val1 = Number(voteValue);
                let val2 = Number(f.rating);
                f.rating = val1 + val2;
                break;
            }
        }
        
        const vote = new Vote();
        vote.id = id;
        vote.value = voteValue;

        const body: string = JSON.stringify(vote);
        const headers = new HttpHeaders({ "Content-Type": "application/json" });

        this._http.put("api/faq/" + id, body, { headers: headers })
            .subscribe(() => {
                this.updateCounter(id, upvote);
            },
                error => console.log(error),
        )
    }

    updateCounter(id: number, upvote: boolean) {
        
    }
}
