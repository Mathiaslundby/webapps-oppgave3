﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Webapps_mappe3.Models;

namespace Webapps_mappe3.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class QuestionsController : Controller
    {
        private readonly FaqContext _context;

        public QuestionsController(FaqContext context)
        {
            _context = context;
        }

        [HttpGet]
        public JsonResult GetQuestions()
        {
            var db = new DbHandler(_context);
            return Json(db.GetQuestions());
        }

        [HttpPost]
        public bool AddQuestion(DomainQuestion question)
        {
            if (ModelState.IsValid)
            {
                var db = new DbHandler(_context);
                return db.AddQuestion(question);
            }
            return false;
        }

        // DELETE: api/Questions/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Question>> DeleteQuestion(int id)
        {
            var question = await _context.Question.FindAsync(id);
            if (question == null)
            {
                return NotFound();
            }

            _context.Question.Remove(question);
            await _context.SaveChangesAsync();

            return question;
        }
    }
}
