﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Webapps_mappe3.Models;
using System.Text.Json;
using Newtonsoft.Json;

namespace Webapps_mappe3.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FaqController : Controller
    {
        private readonly FaqContext _context;

        public FaqController(FaqContext context)
        {
            _context = context;
        }
        
        [HttpGet]
        public JsonResult GetFaq()
        {
            var db = new DbHandler(_context);
            return Json(db.GetFaq());
        }      
        
        [HttpPost]
        public bool AddFaq(DomainFaq faq)
        {
            if(ModelState.IsValid)
            {
                var db = new DbHandler(_context);
                return db.AddFaq(faq);
            }
            return false;
        }

        // DELETE: api/Faqs/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Faq>> DeleteFaq(int id)
        {
            var faq = await _context.Faq.FindAsync(id);
            if (faq == null)
            {
                return NotFound();
            }

            _context.Faq.Remove(faq);
            await _context.SaveChangesAsync();

            return faq;
        }

        [HttpPut("{id}")]
        public JsonResult Put(Vote vote)
        {
            var db = new DbHandler(_context);
            return Json(db.Vote(vote));
        }
    }
}
