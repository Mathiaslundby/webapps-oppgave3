﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Webapps_mappe3.Models
{
    public class DomainQuestion
    {
        public int ID { get; set; }

        [Required]
        [RegularExpression(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$", ErrorMessage = "Ugyldig e-post addresse")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Vennligst skriv inn ditt spørsmål")]
        public string Body { get; set; }

        [Required(ErrorMessage = "Vennligst velg en kategoriId")]
        [RegularExpression(@"^[0-9]{1,}$", ErrorMessage = "KategoriId må være et tall")]
        public string CategoryId { get; set; }

        public string CategoryName { get; set; }
    }
}
