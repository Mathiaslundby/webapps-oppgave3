﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Webapps_mappe3.Models
{
    public class DomainFaq
    {
        public int ID { get; set; }

        [Required(ErrorMessage = "Vennligst skriv et spørsmål")]
        public string Question { get; set; }

        [Required(ErrorMessage = "Vennligst skriv et svar")]
        public string Answer { get; set; }

        [Required(ErrorMessage = "Vennligst skriv en vurdering")]
        public string Rating { get; set; }

        [Required(ErrorMessage = "Vennligst skriv en kategoriId")]
        public string CategoryId { get; set; }
    }
}
