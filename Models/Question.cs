﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Webapps_mappe3.Models
{
    public class Question
    {
        [Key]
        public int ID { get; set; }
        public string Email { get; set; }
        public string Body { get; set; }
        public virtual Category Category { get; set; }
    }
}
