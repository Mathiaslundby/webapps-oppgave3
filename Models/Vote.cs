﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Webapps_mappe3.Models
{
    public class Vote
    {
        public int ID { get; set; }
        public int Value { get; set; }
    }
}
