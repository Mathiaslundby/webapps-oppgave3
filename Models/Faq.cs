﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Webapps_mappe3.Models
{
    public class Faq
    {
        [Key]
        public int ID { get; set; }
        public string Question { get; set; }
        public string Answer { get; set; }
        public int Rating { get; set; }
        public virtual Category Category { get; set; }
    }
}
