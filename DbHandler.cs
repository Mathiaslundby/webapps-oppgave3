﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using Webapps_mappe3.Models;

namespace Webapps_mappe3
{
    public class DbHandler
    {
        private readonly FaqContext _context;

        public DbHandler(FaqContext context)
        {
            _context = context;
        }

        public List<DomainFaq> GetFaq()
        {

            List<DomainFaq> faq = _context.Faq.Include(f => f.Category)
                .Select(f => new DomainFaq()
                {
                    ID = f.ID,
                    Question = f.Question,
                    Answer = f.Answer,
                    Rating = f.Rating.ToString(),
                    CategoryId = f.Category.ID.ToString()
                }).OrderByDescending(f => f.Rating).ToList();
            return faq;
        }

        public bool AddFaq(DomainFaq faq)
        {
            try
            {
                Faq f = new Faq()
                {
                    ID = faq.ID,
                    Question = faq.Question,
                    Answer = faq.Answer,
                    Category = _context.Category.FirstOrDefault(c => c.ID == Int32.Parse(faq.CategoryId)),
                    Rating = Int32.Parse(faq.Rating)
                };

                _context.Add(f);
                _context.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public List<DomainQuestion> GetQuestions()
        {
            List<DomainQuestion> questions = _context.Question.Include(q => q.Category)
                .Select(q => new DomainQuestion()
                {
                    ID = q.ID,
                    Email = q.Email,
                    CategoryId = q.Category.ID.ToString(),
                    CategoryName = q.Category.Name,
                    Body = q.Body
                }).ToList();
            return questions;
        }

        public bool AddQuestion(DomainQuestion question)
        {
            int id = Int32.Parse(question.CategoryId);

            try
            {
                Question q = new Question()
                {
                    ID = question.ID,
                    Email = question.Email,
                    Category = _context.Category.FirstOrDefault(c => c.ID == id),
                    Body = question.Body
                };

                _context.Add(q);
                _context.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public Faq Vote(Vote vote)
        {
            Faq faq = _context.Faq.Find(vote.ID);
            faq.Rating += vote.Value;
            _context.SaveChanges();
            return faq;
        }
    }
}
