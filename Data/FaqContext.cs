﻿using Microsoft.EntityFrameworkCore;

namespace Webapps_mappe3.Models
{
    public class FaqContext : DbContext
    {
        public FaqContext (DbContextOptions<FaqContext> options)
            : base(options) 
        {
            Database.EnsureCreated();
        }

        public DbSet<Category> Category { get; set; }
        public DbSet<Faq> Faq { get; set; }
        public DbSet<Question> Question { get; set; }
    }
}
